# python tasks

## Name
Python tasks for beginners

## Basic Description
Python tasks where 8 tasks need to be solved.
I attach the link:
[pythonworld.ru](https://pythonworld.ru/osnovy/tasks.html)

## Tasks Description
[1](the_simplest_arithmetic_operations) -- The simplest arithmetic operations 

Write a function arithmetic that accepts three arguments: the first 2 - numbers, the third - operation to be performed on them. If third argument + the addition, - subtraction, * multiplication, / division the first on the second number. In other cases to return string "unknown operation".

[2](year_leap) -- Leap year

Write a function is_year_leap that accept one argument -- year and return True. If leap year return True, else return False.

[3](square) -- Square

Write a function square that accept one argument -- the side of square and returns three values (using a tuple): the perimeter of the square, square all the sides and diagonal of the square.

[4](season) -- Seasons

Write a function season that accept one argument —  the number of the month (from 1 to 12) and returns the time of year which this month belongs (winter, spring, summer, autumn).

[5](bank_deposit) -- Bank deposit

The user makes deposit in the amount of a rubels for a period of years at 10% per annum (every year the amount his deposit increases by 10%). This money is added to the amount of the deposit, and there will also be interest on it next year). 
Write a function bank that accepts arguments a and years, and returns that amount that will be on the account.

[6](prime_number) -- Prime numbers

Write a function that accepts one argument — number from 0 to 1000 and returns True if it's simple number, else  -  False.

[7](correct_date) -- Correct date

Write a function date that takes three arguments — day, month, year. Returns True if there is such date in our calendar otherwise False

[8](xor_cipher) -- XOR cipher

Write a function XOR_cipher that takes two arguments: a string which needs encryption and the encrypt key which returns a string 
encryption by the XOR(^) function over symbols of the string with the key.
Also write the XOR_decipher function which recoveries the source string using the encrypted string and key 