def bank(a: int, years: int) -> float:
    summa = a * 1.1 ** years
    return summa


if __name__ == '__main__':
    rubles = 0
    numbers_of_years = 0
    try:
        rubles = int(input())
        numbers_of_years = int(input())
    except ValueError:
        print('Invalid format')

    result = bank(rubles, numbers_of_years)
    print(result)
