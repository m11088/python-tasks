from bank_deposit import bank


def test_bank():
    assert bank(10000, 3) == 13310.000000000004
    assert bank(500, 10) == 1296.8712300500013
