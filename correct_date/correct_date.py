import calendar


def valid_date(day, month, year) -> bool:
    if 0 < month <= 12:
        if 0 < day <= calendar.monthrange(year, month)[1]:
            return True
    return False


if __name__ == '__main__':
    calendar_date = input()
    days, months, years = calendar_date.split()
    result = valid_date(days, months, years)
    print(result)
