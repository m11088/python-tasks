import pytest

from correct_date import valid_date


@pytest.mark.parametrize('day, month, year, excepted', [
        (2, 20, 25555, False),
        (5, 99, 2000, False),
        (9, 10, -555, True),
        (55, 10, 655, False),
        (-8, 12, 899, False),
        (65, 5, 2006, False)])
def test_year_and_month_are_correct(day, month, year, excepted):
    assert valid_date(day, month, year) == excepted


@pytest.mark.parametrize('day, month, year, excepted', [
        (29, 2, 2000, True),
        (29, 2, 2001, False),
        (28, 2, 2000, True),
        (28, 2, 2001, True)])
def test_isleap_year_february(day, month, year, excepted):
    assert valid_date(day, month, year) == excepted


@pytest.mark.parametrize('day, month, year, excepted', [
        (30, 6, 1000, True),
        (31, 7, 4654, True),
        (30, 10, 197, True),
        (31, 11, 899, False)])
def test_existing_date_in_dict_calendar(day, month, year, excepted):
    assert valid_date(day, month, year) == excepted


@pytest.mark.parametrize('day, month, year, excepted', [
        (5, 5, 2007, True),
        (9, 12, 1765, True),
        (31, 6, 555, False)])
def test_other_dates(day, month, year, excepted):
    assert valid_date(day, month, year) == excepted
