def is_prime(number: int) -> str | bool:
    if number > 1000 or number < 0:
        return "Out of range"
    if number == 1:
        return False

    for digit in range(2, number):
        if number % digit == 0:
            return False
    return True


if __name__ == '__main__':
    prime_number = 0
    try:
        prime_number = int(input('Enter number '))
    except ValueError:
        print('Invalid format')

    result = is_prime(prime_number)
    print(result)
