import pytest

from prime_numbers import is_prime


def test_max_out_of_range():
    assert is_prime(1002) == 'out of range'


def test_min_out_of_range():
    assert is_prime(-1) == 'out of range'


def test_number_equally_one():
    assert is_prime(1) is False


@pytest.mark.parametrize('digit, excepted', [
    (141, False),
    (988, False),
    (620, False)])
def test_check_the_numbers_for_False(digit, excepted):
    assert is_prime(digit) == excepted


@pytest.mark.parametrize('digit, excepted', [
    (199, True),
    (977, True),
    (2, True)])
def test_check_the_numbers_for_True(digit, excepted):
    assert is_prime(digit) == excepted
