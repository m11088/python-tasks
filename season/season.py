def season(month: int) -> int | str:
    dict_season = {'зима': [1, 2, 12], 'весна': [3, 4, 5],  'лето': [6, 7, 8], 'осень': [9, 10, 11]}
    for key, value in dict_season.items():
        if month in value:
            return key
    return 'Non existent'


if __name__ == '__main__':
    try:
        number_month = int(input('Enter month number '))
        print(season(number_month))
    except ValueError:
        print('Invalid format')
