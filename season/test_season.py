from season import season


def test_non_existent():
    assert season(0) == 'Non existent'
    assert season(90) == 'Non existent'


def test_season():
    assert season(12) == 'зима'
    assert season(9) == 'осень'
    assert season(7) == 'лето'
    assert season(3) == 'весна'
