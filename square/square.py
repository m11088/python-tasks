from math import sqrt


def square(side: int) -> tuple[int, int, float]:
    perimeter = side * 4
    square_all_side = side ** 2
    diagonal = side * sqrt(2)
    return perimeter, square_all_side, diagonal


if __name__ == '__main__':
    side_square = 0
    try:
        side_square = int(input())
    except ValueError:
        print('Invalid format')

    result = square(side_square)
    print(result)
