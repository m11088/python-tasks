from square import square


def test_square():
    assert square(4) == (16, 16, 5.656854249492381)
    assert square(9) == (36, 81, 12.727922061357857)
    assert square(12) == (48, 144, 16.970562748477143)
