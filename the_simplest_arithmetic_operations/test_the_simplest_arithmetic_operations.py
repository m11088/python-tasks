from the_simplest_arithmetic_operations import arithmetic


def test_Unknown_operation():
    assert arithmetic(3, 'o', 8) == 'Unknown operation'


def test_arithmetic():
    assert arithmetic(3, '+', 8) == 11
    assert arithmetic(90, '/', 9) == 10
    assert arithmetic(8, '-', 9) == -1
    assert arithmetic(87, '*', 9) == 783
