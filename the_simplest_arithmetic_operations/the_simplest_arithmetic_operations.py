def arithmetic(arg_1: int, arg_2: str, arg_3: int) -> float:
    result = 'Unknown operation'
    if arg_2 == '+':
        result = arg_1 + arg_3
    elif arg_2 == '-':
        result = arg_1 - arg_3
    elif arg_2 == '*':
        result = arg_1 * arg_3
    elif arg_2 == '/':
        result = arg_1 / arg_3
    return result


if __name__ == '__main__':
    number_1, number_2 = 0, 0
    operator_arg_2 = ''
    try:
        digit_arg_1, operator_arg_2, digit_arg_3 = input().split()
        number_1 = int(digit_arg_1)
        number_2 = int(digit_arg_3)
    except ValueError:
        print('Invalid format')

    all_the_result = arithmetic(number_1, operator_arg_2, number_2)
    print(all_the_result)
