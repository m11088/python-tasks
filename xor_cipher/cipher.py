import base64


def XOR_cipher(string: str, key: str) -> str:
    result = ''
    counter = 0
    for letter in string:
        if len(key) == counter:
            counter = 0
        symbol = ord(letter) ^ ord(key[counter])
        result += chr(symbol)
        counter += 1

    return result


def XOR_cipher_base64(string: str, key: str) -> str:
    encoded = XOR_cipher(string, key)
    encoded = base64.b64encode(bytes(encoded, 'utf-8'))
    encoded = encoded.decode('utf-8')

    return encoded


def XOR_decipher_base64(string: str, key: str) -> str:
    decode = list(base64.b64decode(bytes(string, 'utf-8')))
    decoded = ''
    for digit in decode:
        decoded += chr(digit)
    decode = XOR_cipher(decoded, key)

    return decode


def XOR_cipher_percent(string: str, key: str):
    cipher = ""
    key_len = len(key)
    for i, letter in enumerate(string):
        symbol = ord(letter) ^ ord(key[i % key_len])
        cipher += chr(symbol)
    cipher = base64.b64encode(bytes(cipher, 'utf-8'))
    cipher = cipher.decode('utf-8')
    return cipher


def _input_message_key() -> tuple[str, str]:
    message = input('Enter message: ')
    key = input('Enter key: ')
    return message, key


if __name__ == '__main__':
    message = ''
    key_to_message = ''
    choice_function = 1
    while choice_function != '0':
        choice_function = input('Choice:\n 1 - cipher \n 2 - decipher \n 0 - exit \n')
        if choice_function == '1':
            message, key = _input_message_key()
            encrypted = XOR_cipher_base64(message, key)
            print(encrypted)
        elif choice_function == '2':
            encrypted_message, key = _input_message_key()
            decryption = XOR_decipher_base64(encrypted_message, key)
            print(decryption)
