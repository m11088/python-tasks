from cipher import XOR_cipher_base64
from cipher import XOR_decipher_base64


def test_values_to_base64():
    assert XOR_decipher_base64(XOR_cipher_base64('test', 'key'), 'key') == 'test'
    assert XOR_decipher_base64(XOR_cipher_base64('message', 'key'), 'key') == 'message'
    assert XOR_decipher_base64(XOR_cipher_base64('cipher', 'key'), 'key') == 'cipher'
    assert XOR_decipher_base64(XOR_cipher_base64('decipher', 'key'), 'key') == 'decipher'
