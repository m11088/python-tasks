import pytest

from year_leap import is_year_leap


@pytest.mark.parametrize('year, excepted', [
    (400, True),
    (1900, False),
    (200, False),
    (1924, True),
    (2000, True),
    (2080, True),
    (1956, True)])
def test_is_year_leap(year, excepted):
    assert is_year_leap(year) == excepted
