def is_year_leap(year: int):
    return bool(not year % 4 and year % 100 or not year % 400)


if __name__ == '__main__':
    year_leap = 0
    try:
        year_leap = int(input('Enter year leap '))
    except ValueError:
        print("It's not a digit")

    result = is_year_leap(year_leap)
    print(result)
